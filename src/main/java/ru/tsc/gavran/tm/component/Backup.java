package ru.tsc.gavran.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gavran.tm.api.service.IPropertyService;
import ru.tsc.gavran.tm.command.data.BackupLoadCommand;
import ru.tsc.gavran.tm.command.data.BackupSaveCommand;

import java.util.concurrent.*;

public class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();
    private final Bootstrap bootstrap;

    private final int INTERVAL;

    public Backup(@NotNull final Bootstrap bootstrap, @NotNull final IPropertyService propertyService) {
        this.bootstrap = bootstrap;
        this.INTERVAL = propertyService.getBackupInterval();
    }


    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        bootstrap.parseCommand(BackupSaveCommand.BACKUP_SAVE);
    }

    public void load() {
        bootstrap.parseCommand(BackupLoadCommand.BACKUP_LOAD);
    }
}
