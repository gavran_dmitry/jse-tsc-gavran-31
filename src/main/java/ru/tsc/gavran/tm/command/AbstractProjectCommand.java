package ru.tsc.gavran.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.exception.empty.EmptyNameException;
import ru.tsc.gavran.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.gavran.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(@Nullable Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

    protected Project add(@Nullable final String name, @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return new Project(name, description);
    }

}
